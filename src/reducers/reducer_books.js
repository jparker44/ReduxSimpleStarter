export default function() {
    return [
        {title: 'American Gods', pages: 231 },
        {title: 'Juggler of Worlds', pages: 283 },
        {title: 'God Emperor of Dune', pages: 455 },
        {title: 'Player of Games', pages: 387 },
        {title: 'Foundation and Empire', pages: 519 }
    ]
}
